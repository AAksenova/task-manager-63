package ru.t1.aksenova.tm.repository;

import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Project;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final  ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("One test project", Status.IN_PROGRESS));
        add(new Project("Two test project", Status.NOT_STARTED));
        add(new Project("Three test project", Status.COMPLETED));

    }

    public void create() {
        final String size = String.valueOf(projects.size()+1);
        add(new Project("New Project " + size, Status.NOT_STARTED));
    }

    public void add(final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(final Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public void removeById(final String id) {
        projects.remove(id);
    }

}
