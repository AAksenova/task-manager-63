package ru.t1.aksenova.tm.repository;

import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private static final  TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("Alfa task", Status.IN_PROGRESS));
        add(new Task("Beta task", Status.COMPLETED));
        add(new Task("Gamma task", Status.NOT_STARTED));

    }

    public void create() {
        final String size = String.valueOf(tasks.size()+1);
        add(new Task("New Task " + size, Status.NOT_STARTED));
    }

    public void add(final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(final Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(final String id) {
        return tasks.get(id);
    }

    public void removeById(final String id) {
        tasks.remove(id);
    }

}
