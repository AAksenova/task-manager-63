package ru.t1.aksenova.tm.enumerated;

import lombok.Getter;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status( final String displayName) {
        this.displayName = displayName;
    }

    public static String toName( final Status status) {
        if (status == null) return "";
        return status.displayName;
    }

    public static Status toStatus( final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

}
