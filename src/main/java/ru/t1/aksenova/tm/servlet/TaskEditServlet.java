package ru.t1.aksenova.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.repository.ProjectRepository;
import ru.t1.aksenova.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        req.setAttribute("task", TaskRepository.getInstance().findById(id));
        req.setAttribute("status", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String projectId = req.getParameter("projectId");
        final String description = req.getParameter("description");
        final String statusValue = req.getParameter("status");
        final Status status = Status.valueOf(statusValue);

        final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setProjectId(projectId);
        task.setDescription(description);
        task.setStatus(status);

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}
