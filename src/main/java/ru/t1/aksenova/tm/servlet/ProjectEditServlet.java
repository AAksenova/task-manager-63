package ru.t1.aksenova.tm.servlet;

import lombok.SneakyThrows;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        req.setAttribute("project", ProjectRepository.getInstance().findById(id));
        req.setAttribute("status", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final String statusValue = req.getParameter("status");
        final Status status = Status.valueOf(statusValue);

        final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setStatus(status);

        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");

    }
}
