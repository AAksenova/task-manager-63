package ru.t1.aksenova.tm.model;


import lombok.Getter;
import lombok.Setter;
import ru.t1.aksenova.tm.enumerated.Status;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Project {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    public Project() {
    }

    public Project (
            final String name,
            final Status status
    ) {
        this.name = name;
        this.status = status;
    }

}
